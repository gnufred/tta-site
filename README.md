# Tabletop Arena public website

> This project is abandoned. Public archive at https://gnufred.gitlab.io/tta-site. Some links might not work.

This repository contains static files that serve https://ttarena.tk

https://ttarena.tk/dnd5e/ is generated using [MkDocs](https://www.mkdocs.org/).

Everything is licensed to be open and reusable. Read [LICENSE.md](LICENSE.md) for more information.
